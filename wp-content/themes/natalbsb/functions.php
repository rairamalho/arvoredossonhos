<?php
if (function_exists('add_theme_support')){
	add_theme_support('post-thumbnails');
}


/**
 * Inserindo CSSs e JSs no WP
 */
function enqueue_scripts() {
	$template_url = get_template_directory_uri();

	wp_enqueue_style( 'fontawesome-css', $template_url . '/assets/fonts/all.css', array(), null, 'all' );
	wp_enqueue_style( 'bootstrap-min-css', $template_url . '/assets/css/bootstrap.min.css', array(), null, 'all' );
	wp_enqueue_style( 'bootstrap-grid-css', $template_url . '/assets/css/bootstrap-grid.min.css', array(), null, 'all' );
	wp_enqueue_style( 'estilo-css', $template_url . '/assets/css/estilo.css?' . md5(date('i')), array(), null, 'all' );
	
	// jQuery.
	wp_enqueue_script( 'jquery' );
	// JS
	wp_enqueue_script( 'sweetalert-js', '//cdn.jsdelivr.net/npm/sweetalert2@9', array( 'jquery' ), null, true );
	wp_enqueue_script( 'mask-js', $template_url . '/assets/js/jquery.mask.min.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'validate-js', $template_url . '/assets/js/jquery.validate.min.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'bootstrap-js', $template_url . '/assets/js/bootstrap.min.js?ver=' . md5(date('i')), array( 'jquery' ), null, true );
	wp_enqueue_script( 'main-js', $template_url . '/assets/js/main.js?ver=' . md5(date('i')), array( 'jquery' ), null, true );
}
add_action( 'wp_enqueue_scripts', 'enqueue_scripts', 1 );

add_filter('show_admin_bar', '__return_false');

add_theme_support( 'responsive-embeds' );

remove_filter('the_content', 'wpautop');