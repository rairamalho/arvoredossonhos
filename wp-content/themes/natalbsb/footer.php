    <footer>
        <div class="container">
            <div class="row">
                <div class="col-12 comp_face">
                    <a href="https://www.facebook.com/sharer/sharer.php?u=http://df.gov.br/arvoredossonhos" target="_blank"><i class="fab fa-facebook-f"></i> Compartilhar no Facebook</a>
                </div>
            	<div class="col-12 col-sm-6 col-lg-4">
            		<div class="redes">
                        <a href="https://www.instagram.com/gov_df/" target="_blank"><i class="fab fa-instagram"></i></a>
                        <a href="https://www.facebook.com/govdf" target="_blank" class="face"><i class="fab fa-facebook-f"></i></a>
                        <a href="https://twitter.com/Gov_DF" target="_blank"><i class="fab fa-twitter"></i></a>
                        <a href="https://www.youtube.com/user/govdf" target="_blank" class="tube"><i class="fab fa-youtube"></i></a>
            		</div>
            	</div>
            	<div class="col-12 col-sm-6 col-lg-8 right"><p>A cada sonho compartilhado uma muda de árvore nativa será plantada no Distrito Federal</p><p>O GDF deseja um Feliz Natal e um próspero Ano Novo</p></div>
            </div>
        </div>
    </footer>

    <!-- Preloader -->
    <div class="loading">
        <div class="gif"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/loading.gif"></div>
    </div>

	<?php wp_footer(); ?>

    <script type="text/javascript">
        <?php if(isset($_GET['status']) && $_GET['status'] == '1'){ ?>
            <?php if(isset($_GET['cod']) && $_GET['cod'] != ''){ ?>
                Swal.fire({
                  title: '<strong>Obrigado!</strong>',
                  icon: 'success',
                  html:
                    'Seu sonho foi cadastrado com sucesso.',
                  showCancelButton: true,
                  focusConfirm: false,
                  confirmButtonText:
                    '<i class="fab fa-facebook-f"></i> Compartilhar meu sonho!',
                  confirmButtonAriaLabel: 'Thumbs up, great!',
                  cancelButtonText:
                    'Fechar',
                  cancelButtonAriaLabel: 'Thumbs down'
                }).then((result) => {
                  if (result.isConfirmed) {
                    window.open(
                      'https://www.facebook.com/sharer/sharer.php?u=<?php echo $_GET['cod']; ?>',
                      '_blank' // <- This is what makes it open in a new window.
                    );
                    location.href = '<?php echo $_GET['cod']; ?>';
                  } else if (result.isDenied) {
                    //Swal.fire('Changes are not saved', '', 'info')
                  }
                })
            <?php  }else{ ?> 
                Swal.fire({
                  title: '<strong>Obrigado!</strong>',
                  icon: 'success',
                  html:
                    'Seu sonho foi cadastrado com sucesso.',
                  showCancelButton: true,
                  focusConfirm: false,
                  confirmButtonText:
                    '<i class="fab fa-facebook-f"></i> Compartilhar no Facebook!',
                  confirmButtonAriaLabel: 'Thumbs up, great!',
                  cancelButtonText:
                    'Fechar',
                  cancelButtonAriaLabel: 'Thumbs down'
                }).then((result) => {
                  if (result.isConfirmed) {
                    window.open(
                      'https://www.facebook.com/sharer/sharer.php?u=http://df.gov.br/arvoredossonhos',
                      '_blank' // <- This is what makes it open in a new window.
                    );
                  } else if (result.isDenied) {
                    //Swal.fire('Changes are not saved', '', 'info')
                  }
                })
                /*
                Swal.fire(
                  'Obrigado!',
                  'Seu sonho foi cadastrado com sucesso.',
                  'success'
                )
                */
            <?php } ?>
                
        <?php } ?>

        <?php if(isset($_GET['status']) && $_GET['status'] == '2'){ ?>
            Swal.fire(
              'Erro não esperado!',
              'Favor tentar novamente em algums minutos.',
              'error'
            )
        <?php } ?>
    </script>
</body>
</html>