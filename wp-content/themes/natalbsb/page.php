<?php get_header(); ?>

<section class="interna">
	<div class="conteiner">
		<div class="row">
			<div class="col-10 offset-1 col-sm-6 offset-sm-3">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<article>
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>
					</article>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>