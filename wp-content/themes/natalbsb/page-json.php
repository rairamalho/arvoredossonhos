<?php 
 
// The Query
$the_query = new WP_Query( 'post_type=post&posts_per_page=124' );

$array = array();
 
// The Loop
if ( $the_query->have_posts() ) {
    //echo '[';
    while ( $the_query->have_posts() ) {
        $the_query->the_post();

        $content = preg_replace( "/(\r\n)|\n|\r/", "", nl2br(get_the_content()) );
            $array[] = array(
                'id' => get_the_ID(),
                'foto' => get_the_post_thumbnail_url(get_the_ID(), 'thumbnail'),
                'nome' => get_the_title(),
                'sonho' => get_the_content()
              );

        
    }
    //echo ']';
} else {
    // no posts found
}
/* Restore original Post Data */
wp_reset_postdata();

print_r(json_encode($array));

?>