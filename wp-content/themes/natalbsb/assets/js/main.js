// JavaScript Document

jQuery(document).ready(function(e) {
	//============================================================================================================
	
	jQuery('.abrirF').on('click', function(e) {
		e.preventDefault();
		jQuery(".formulario").slideDown();
		jQuery('html,body').animate({
	        scrollTop: jQuery(".formulario").offset().top
	    }, 'slow');
	});

	//============================================================================================================

	jQuery('.box').on('click', function(e) {
		jQuery(".box").hide();
	});
	//============================================================================================================

	jQuery.ajax({
		type: 'POST',
		url: 'json',
		success: function(data) {
			var obj = JSON.parse(data);
			var tam = obj.length - 1;
	
			jQuery('.bt').on('click', function(e) {
				jQuery(".box").hide();
				var id = jQuery(this).attr('id').substring(2);
				var tamanho = jQuery(".box").height();

				var foto = '';
				var nome = '';
				var sonho = '';

				if(id <= tam){
					foto = obj[id].foto;
					nome = obj[id].nome;
					sonho = obj[id].sonho;
				}else{
					var aux = Numbers(0,tam)
					foto = obj[aux].foto;
					nome = obj[aux].nome;
					sonho = obj[aux].sonho;
				}

				jQuery(".box .nome").html(nome);
				jQuery(".box .sonho").html(sonho);
				jQuery('.box .foto img').attr('src', foto);

				var pos_left = e.pageX - 290;
				var pos_top = e.pageY - tamanho + 15;

				if(pos_left <= -15){
					pos_left = -15;
				}

				jQuery(".box").css({
		            	    left: pos_left,
		            	    top: pos_top
		            	})

				jQuery(".box").fadeIn('slow');
			});
		}
    });
	
});

jQuery(window).load(function(){
	//
});

jQuery(window).resize(function(){
	//
	jQuery(".box").hide();
});

function Numbers(min,max){
	return Math.floor(Math.random()*(max-min+1)+min);
	//console.log(Math.floor(Math.random()*(max-min+1)+min));
}