<?php 
if ( !empty( $_POST ) ) {
	$nome = esc_attr( $_POST['nome'] );
	$sonho = esc_attr( $_POST['sonho'] );

	$url = home_url();

	$defaults = array(
        'post_status' => 'pending',
        'post_type' => 'post',
        'post_author' => 2,
        'post_title' => $nome,
        'post_content' => $sonho,
    );

	$post_id = wp_insert_post( $defaults );

	if(isset($_FILES['upload_image'])){

		if ( ! function_exists( 'wp_handle_upload' ) ) {
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
		}
		$uploadedfile = $_FILES['upload_image'];
		$upload_overrides = array( 'test_form' => false );
		$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
		if ( $movefile && !isset( $movefile['error'] ) ) {
		    $caminho_absoluto = $movefile['file'];
		    $caminho_relativo = $movefile['url'];
		    $tipo_imagem = $movefile['type'];

		    $attachment = array(
	            'guid' => basename( $caminho_absoluto ),
	            'post_mime_type' => $tipo_imagem,
	            'post_title' => preg_replace('/\.[^.]+$/', '', basename( $caminho_absoluto ) ),
	            'post_content' => '',
	            'post_status' => 'publish'
	        );
	        $attach_id = wp_insert_attachment( $attachment, $caminho_absoluto, $post_id );

			require_once( ABSPATH . 'wp-admin/includes/image.php' );
	        $attach_data = wp_generate_attachment_metadata( $attach_id, $caminho_absoluto );
			wp_update_attachment_metadata( $attach_id, $attach_data );

			set_post_thumbnail( $post_id, $attach_id );

			//header('Location: '.$url.'?status=1&cod='.get_permalink($post_id));
			header('Location: '.$url.'?status=1&cod=');

	}else{
		header('Location: '.$url.'?status=1&cod=');
	}
	}else{
		header('Location: '.$url.'?status=2&err=1');
	}
}else{
	header('Location: '.$url.'?status=2');
}