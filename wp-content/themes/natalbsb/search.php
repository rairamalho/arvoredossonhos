<?php get_header(); ?>
<section class="interna">
	<div class="conteiner">
		<div class="row">
			<div class="col-12 col-sm-6 offset-sm-3">
				<h1><?php printf( esc_html__( 'Busca por: %s', 'blankslate' ), get_search_query() ); ?></h1>
				<div class="busca">
					<form role="search" method="get" id="searchform" action="<?php echo home_url(''); ?>" >
						<label for="s">Pesquise um Sonho:
                        <input type="hidden" name="post_type" value="post">
						<input type="text" value="" name="s" id="s" />
						<input type="submit" id="searchsubmit" value="Buscar" />
					</form>
				</div>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<div class="item">
						<?php if ( has_post_thumbnail() ) : ?>
							<div class="foto"><?php the_post_thumbnail('thumbnail'); ?></div>
						<?php endif; ?>
						<div class="texto">
							<h2><?php the_title(); ?></h2>
							<?php the_content(); ?>
						</div>
					</div>
				<?php endwhile; endif; ?>
                <div class="center" style="text-align: center;">
                    <?php if(function_exists('wp_paginate')) {
                        wp_paginate();
                    } ?>
                </div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
