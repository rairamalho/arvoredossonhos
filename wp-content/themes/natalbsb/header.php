<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-185670658-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-185670658-1');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-MWWG7C1T6E"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-MWWG7C1T6E');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '2552851198348007');
      fbq('track', 'PageView');
    </script>
    <!-- End Facebook Pixel Code -->
    <?php if(isset($_GET['status']) && $_GET['status'] == '1'){ ?>
        <!-- Facebook Pixel Code -->
        <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '2552851198348007');
          fbq('track', 'PageView');
          fbq('track', 'PreencheuFormulário');
        </script>
        <!-- End Facebook Pixel Code -->
    <?php } ?>

    <!-- Hotjar Tracking Code for https://www.arvoredossonhosgdf.com.br/ -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:2168089,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

	<meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">

	<?php wp_head(); ?>
</head>
<body>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=2552851198348007&ev=PageView&noscript=1"
    /></noscript>
    <?php if(isset($_GET['status']) && $_GET['status'] == '1'){ ?>
        <noscript><img height="1" width="1" style="display:none"
          src="https://www.facebook.com/tr?id=2552851198348007&ev=PageView&noscript=1"
        /></noscript>
    <?php } ?>
	<div class="box">
		<div class="box-topo"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/box-topo.png"></div>
		<div class="box-meio">
			<div class="conteudo">
				<div class="foto"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/guiafoto.jpg"></div>
				<div class="texto">
					<div class="nome">Maria Eduarda Conceição</div>
					<div class="sonho">Meu sonho é que o ano de 2021 seja repleto de novas oportunidades</div>
				</div>
			</div>
		</div>
		<div class="box-topo"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/box-base.png"></div>
	</div>

    <!-- TOPO E MENU -->
    <header>
        <!-- MENU -->
        <nav class="navbar navbar-expand-lg navbar-dark">
            <div class="container">
                <div class="row">
                	<a class="navbar-brand" href="<?php echo home_url(''); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png"></a>

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07" aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarsExample07">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item"><a class="nav-link" href="<?php echo home_url('sonhos'); ?>">Sonhos</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo home_url('a-acao'); ?>">A ação</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </header>