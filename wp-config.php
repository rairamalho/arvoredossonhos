<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'natalbsb' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'sw]V8VGY 6RV;4!ip^D&^z3glz*<p[%,o-=BX|8yrg$AwZgjI-m,MucckhTA+[#.');
define('SECURE_AUTH_KEY',  '!Q1YzcC2_b?^k;>y5dKl6+ws<j@.(TOL|5C k;|;|,kk*[}:GFk-GhmkVL4IEJ* ');
define('LOGGED_IN_KEY',    '`;hwWVx-3SQYVvfpK1?Q,E=cNdoa=3QU9hqr:M58xyn%~~P*}5l>{H:Z~5g6Ym@P');
define('NONCE_KEY',        'F.k2,0r$brHG[7cVvIj0,T!F_`+H,e0cZR[${k|mT*VefvtQtZv<3G2A01G$42q%');
define('AUTH_SALT',        't/XRASLY9czX`%g`GaMT|*iA/w|<Hh*&ziQj[`mC##R4Z Ndn BKc2`Hf5^`CM (');
define('SECURE_AUTH_SALT', 'N &>+]7FNUz -WZ+Bw`)GM8yzO5<QLs3P-v>7?Z2l[BSF8Wl^B`D&}muo?,ki`4j');
define('LOGGED_IN_SALT',   '|,Dt3C]W-1N)*OO]WWm$?7; |+x?Xn2h|[F4?-5@9-whhGP_qNtz2W;OU9{P$+xb');
define('NONCE_SALT',       ')}2Z8ClFIDhHt/}Au{f`>g/#PL@P~t!|1lyC/+|HRea#,>(=.o7|59<?E9aoc47i');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'cid_ntb_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
define( 'FS_METHOD', 'direct' );
